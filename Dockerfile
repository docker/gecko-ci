FROM cirrusci/flutter:stable
MAINTAINER elois <c@elo.tf>
LABEL description="CI for Ğecko project"

ENV DEBIAN_FRONTEND noninteractive
#ENV ANDROID_NDK_VERSION 25.1.8937393

# Install Android NDK
#RUN yes | sdkmanager "ndk;$ANDROID_NDK_VERSION"
#ENV ANDROID_NDK_HOME ${ANDROID_SDK_ROOT}/ndk/${ANDROID_NDK_VERSION}
